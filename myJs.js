const hitNegara = "https://insw-dev.ilcs.co.id/n/negara?ur_negara=";
const hitPelabuhan = "https://insw-dev.ilcs.co.id/n/pelabuhan?kd_negara=";
const hitDetail = "https://insw-dev.ilcs.co.id/n/barang?hs_code=";
const hitTarif = "https://insw-dev.ilcs.co.id/n/tarif?hs_code=";

let  hitApi = "";
let kodeNegara= "";
let dataCukai = 0;

async function getNegara(e) {
    hitApi = hitNegara + e;
    if (e.length == 3){
        const response = await fetch(hitApi);
        const jsonResponse = await response.json();

        if (jsonResponse.code === "200") {
            const dataNegara =jsonResponse.data[0].ur_negara;
            kodeNegara=jsonResponse.data[0].kd_negara;
            document.getElementById('negara').value = dataNegara;
        }
    }
}
async function getPelabuhan(e) {
    hitApi = hitPelabuhan + kodeNegara + '&ur_pelabuhan=' + e;
    if (e.length == 3){
        const response = await fetch(hitApi);
        const jsonResponse = await response.json();
        if (jsonResponse.code === "200") {
            const dataPelabuhan = jsonResponse.data[0].ur_pelabuhan;
            document.getElementById('pelabuhan').value = dataPelabuhan;
        }
    }
}
async function getDetail(e){
    hitApi = hitDetail + e;
    const response = await fetch(hitApi);
    const jsonResponse = await response.json();
    if (jsonResponse.code === "200") {
        const dataDetail = jsonResponse.data[0].sub_header + ' ' + jsonResponse.data[0].uraian_id;
        document.getElementById('detailBarang').value = dataDetail;
    }
    else{
        document.getElementById('detailBarang').value = "";
    }
    await getCukai(e);
}
async function getCukai(e){
    hitApi = hitTarif + e;
    const response = await fetch(hitApi);
    const jsonResponse = await response.json();
    if (jsonResponse.code === "200") {
        let dataCukai = jsonResponse.data[0].bm;
        document.getElementById('persenBM').value = dataCukai;
    }
    else{
        document.getElementById('persenBM').value = "";
    }
}
async function getTotal(e){
    let dataTotal = e * dataCukai;
    if (dataTotal) {
        document.getElementById('tarifBM').value = dataTotal;
    } else {
        document.getElementById('tarifBM').value = "";
    }
}